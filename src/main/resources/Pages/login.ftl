<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html>
  <head>

 <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <link href="/Pages/login.css" rel="stylesheet" id="login-css">
  <link href="/Pages/error.css" rel="stylesheet" id="error-css">
<!------ Include the above in your HEAD tag ---------->
  </head>
<body id="LoginForm">
<div class="container">
<h1 class="form-heading">login Form</h1>
<div class="login-form">
<div class="main-div">
    <div class="panel">
   <h2>Login</h2>
   <p>Please enter your email and password</p>
   </div>
    <form id="Login" action="/login" method="post" id="Login">
    <label class="error"><h4>${error!""}</h4></label>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form-group">


            <input type="email" class="form-control" name="username" id="email" placeholder="Email Address" required value=${LoginForm.getEmail()!""}>

        </div>

        <div class="form-group">

            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required value=${LoginForm.getPassword()!""}>

        </div>
        <div class="forgot">
        <a href="/register">Create Account</a>
</div>
        <button type="submit" class="btn btn-primary">Login</button>

    </form>
    </div>
</div></div></div>


</body>
</html>