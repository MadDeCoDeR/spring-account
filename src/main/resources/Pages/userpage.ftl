<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<html>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="/Pages/error.css" rel="stylesheet" id="error-css">
</head>

<body>
<hr>
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1>User Page</h1></div>
    	<div class="col-sm-2"><a href="/logout" class="pull-right">Logout</a></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->

          
        </div><!--/col-3-->
    	<div class="col-sm-9">

              
          <div class="tab-content">
             <div class="tab-pane active" id="messages">
               
               <h2></h2>
               
               <hr>
                  <form class="form" action="/update/${User.getAccountID()!""}" method="post" id="userForm">
                  <label class="error"><h4>${error!""}</h4></label>
                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="first_name"><h4>First name</h4></label>
                              <input type="text" class="form-control" name="firstName" id="firstName" placeholder="first name" title="enter your first name if any." value=${User.getFirstName()!""}>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Last name</h4></label>
                              <input type="text" class="form-control" name="lastName" id="lastName" placeholder="last name" title="enter your last name if any." value=${User.getLastName()!""}>
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4>Phone</h4></label>
                              <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any." value=${User.getPhone()!""}>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email." value=${User.getEmail()!""}>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Company</h4></label>
                              <input type="text" class="form-control" id="company" name="company" placeholder="somewhere" title="enter a Company Name" value=${User.getCompany()!""}>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="password"><h4>Old Password</h4></label>
                              <input type="password" class="form-control" name="olpassword" id="olpassword" placeholder="password" pattern="(?=.*[A-Za-z])(?=.*\d)(?=.*[!@#$%^&*_=+-]).{8}$" title="Must contain at least one letter, one number, one symbol and has minimum length 8 characters">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="password2"><h4>New Password</h4></label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="password2" title="enter your new password.">
                          </div>
                      </div>
                      <div class="col-xs-6">
                            <label for="password2"><h4>Verify Password</h4></label>
                              <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="password2" title="verify your new password.">
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
               
             </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    <script src="/Pages/password.js"></script>
    <script src="/Pages/userpage.js"></script>
</body>
</html>                      