package com.account.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.account.domain.Account;

public interface AccountRepository extends JpaRepository<Account,Long> {
	
	Optional<Account> getAccountByEmail(String email);

}
