package com.account.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.account.model.LoginResponse;
import com.account.service.AccountService;
@Component
public class AccountAuthenticationManager implements AuthenticationProvider {
	@Autowired
	private AccountService authenticationService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		LoginResponse loginResponse = authenticateUser(authentication);
        if (loginResponse == null){
            return null;
        }
        return new UsernamePasswordAuthenticationToken(loginResponse.getUsername(), loginResponse.getPassword(), loginResponse.getRoles());
	}

	private LoginResponse authenticateUser(Authentication authentication) {
		String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        return authenticationService.login(username, password);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
