package com.account.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.account.exception.AccountNotFoundException;
import com.account.model.LoginModel;

@Controller
public class LoginController {
	
	@GetMapping("/login")
	public String login(Model model) {
		model.addAttribute("LoginForm",new LoginModel());
		return "Pages/login";
	}
	
	 @GetMapping("/errorcred")
	    public String HandleError(Model model){
	        model.addAttribute("error","Wrong Email/Password");
	        model.addAttribute("LoginForm",new LoginModel());
	        return "Pages/login";
	    }
	 
	 @ExceptionHandler(AccountNotFoundException.class)
	    public String handleMissingAccount(HttpServletRequest request, RedirectAttributes reAttr,RuntimeException e){
	        reAttr.addFlashAttribute("error","This Account doesn't exist");
	        return "redirect:/login";
	    }
	 
	 @GetMapping("/logout")
	    public String doLogout(HttpServletRequest request){
	        try {
	            request.logout();
	            HttpSession session = request.getSession();
	            session.invalidate();
	        }catch (ServletException e) {
	            System.err.println(e.getMessage());
	        }
	        return "redirect:/";
	    }

}
