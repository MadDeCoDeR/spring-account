package com.account.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.account.model.AccountModel;
import com.account.model.LoginModel;
import com.account.service.AccountService;

@Controller
public class RegisterController {
	
	@Autowired
	private AccountService service;
	
	@GetMapping("/register")
	public String getRegisterPage(Model model) {
		model.addAttribute("RegisterForm",new AccountModel());
		return "Pages/register";
	}
	
	@PostMapping("/register")
	public String doRegister(Model model, @Valid @ModelAttribute("RegisterForm") AccountModel RegisterForm, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("error","Email already exists");
			return "Pages/register";
		}
		service.Create(RegisterForm);
		LoginModel loginmodel = new LoginModel();
		loginmodel.setEmail(RegisterForm.getEmail());
		loginmodel.setPassword(RegisterForm.getPassword());
		model.addAttribute("LoginForm",loginmodel);
		return "Pages/login";
	}
	

}
