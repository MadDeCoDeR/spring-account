package com.account.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.account.exception.AccountNotFoundException;
import com.account.model.AccountModel;
import com.account.service.AccountService;

@Controller
public class UpdateController {
	
	@Autowired
	private AccountService service;
	
	@PostMapping("/update/{id}")
	public String doUpdate(@PathVariable long id,Model model, @Valid @ModelAttribute("UpdateForm") AccountModel updateForm, BindingResult result) {
		
		String email=service.findAccountById(id).getEmail();
		String password=service.findAccountById(id).getPassword();
		updateForm.setAccountID(id);
		
		if (result.hasErrors() && !updateForm.getEmail().equals(email)) {
			return handleError("Email already exists",updateForm,model);
		}
		if (updateForm.getPassword() == "") {
			updateForm.setPassword(password);
		}else {
			if (!updateForm.getOlpassword().equals(password)) {
				return handleError("Old Password is wrong",updateForm,model);
			}
		}
		service.Update(updateForm);
		model.addAttribute("User",updateForm);
		return "Pages/userpage";
	}
	
	private String handleError(String message,AccountModel account, Model model) {
		model.addAttribute("error",message);
		model.addAttribute("User",account);
		return "Pages/userpage";
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
    public String handleMissingAccount(HttpServletRequest request, RedirectAttributes reAttr,RuntimeException e){
        reAttr.addFlashAttribute("error","This Account doesn't exist");
        return "redirect:/update";
    }

}
