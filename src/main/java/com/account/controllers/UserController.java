package com.account.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.account.exception.AccountNotFoundException;
import com.account.model.AccountModel;
import com.account.service.AccountService;

@Controller
public class UserController {
	
	@Autowired
	private AccountService service;
	
	@GetMapping("/userPage")
	public String getUserPage(Model model) {
		
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		AccountModel account = service.findAccountByEmail(email);
		model.addAttribute("User",account);
		return "Pages/userpage";
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
    public String handleMissingAccount(HttpServletRequest request, RedirectAttributes reAttr,RuntimeException e){
        reAttr.addFlashAttribute("error","This Account doesn't exist");
        return "redirect:/login";
    }

}
