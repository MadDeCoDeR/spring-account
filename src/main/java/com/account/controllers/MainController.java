package com.account.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.account.exception.AccountNotFoundException;
import com.account.model.AccountModel;
import com.account.service.AccountService;

@Controller
public class MainController {
	
	@Autowired
	private AccountService service;
	
	@GetMapping("/")
	public String Home() {
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		AccountModel account = service.findAccountByEmail(email);
		return "redirect:/userPage";
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
    public String handleMissingAccount(HttpServletRequest request, RedirectAttributes reAttr,RuntimeException e){
		return "redirect:/login";
    }

}
