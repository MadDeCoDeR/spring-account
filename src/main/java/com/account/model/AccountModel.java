package com.account.model;

import java.util.Base64;

public class AccountModel {
	
	private long accountID;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String company;
	private String password;
	private String olpassword;
	
	public long getAccountID() {
		return accountID;
	}
	public void setAccountID(long accountID) {
		this.accountID = accountID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOlpassword() {
		return olpassword;
	}
	public void setOlpassword(String olpassword) {
		this.olpassword = olpassword;
	}
	

}
