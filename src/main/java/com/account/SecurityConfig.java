package com.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.account.security.AccountAuthenticationManager;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private AccountAuthenticationManager accountauthenticationmanager;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/userPage",true)
                .failureUrl("/errorcred")
                .and()
                .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/logout")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/","/login","/errorcred","/register").permitAll()
                .antMatchers("/logout").anonymous()
                .antMatchers("/userPage/*","/userPage","/update","/update/*").hasAuthority("USER")
                .and()
                .authenticationProvider(accountauthenticationmanager)
                .headers()
                .frameOptions()
                .sameOrigin();
    }

}
