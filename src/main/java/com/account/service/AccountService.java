package com.account.service;

import com.account.model.AccountModel;
import com.account.model.LoginResponse;

public interface AccountService {
	
	AccountModel findAccountById(long Id);
	
	AccountModel findAccountByEmail(String email);
	
	void Create(AccountModel model);
	
	void Update(AccountModel model);
	
	LoginResponse login(String email,String password);

}
