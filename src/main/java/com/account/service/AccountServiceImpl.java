package com.account.service;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.account.domain.Account;
import com.account.exception.AccountNotFoundException;
import com.account.mapper.ModelMapper;
import com.account.model.AccountModel;
import com.account.model.LoginResponse;
import com.account.repository.AccountRepository;
@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountRepository repository;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public AccountModel findAccountById(long Id) {
		return mapper.toModel(repository.findById(Id).orElseThrow(AccountNotFoundException::new));
	}

	@Override
	public AccountModel findAccountByEmail(String email) {
		return mapper.toModel(repository.getAccountByEmail(email).orElseThrow(AccountNotFoundException::new));
	}

	@Override
	public void Create(AccountModel model) {
		Account account = new Account(model.getFirstName(),
				model.getLastName(),
				model.getEmail(),
				model.getPhone(),
				model.getCompany(),
				model.getPassword());
		
		repository.saveAndFlush(account);
		
	}

	@Override
	public void Update(AccountModel model) {
		Account account = repository.findById(model.getAccountID()).orElseThrow(AccountNotFoundException::new);
		account.setFirstName(model.getFirstName());
		account.setLastName(model.getLastName());
		account.setEmail(model.getEmail());
		account.setPhone(model.getPhone());
		account.setCompany(model.getCompany());
		account.setPassword(model.getPassword());
		
		repository.saveAndFlush(account);
	}

	@Override
	public LoginResponse login(String email, String password) {
		Account account = repository.getAccountByEmail(email).orElseThrow(AccountNotFoundException::new);
		if (account.getPassword().equals(password)) {
			return new LoginResponse(email,password,Collections.singletonList(new SimpleGrantedAuthority("USER")));
		}
		return null;
	}

}
