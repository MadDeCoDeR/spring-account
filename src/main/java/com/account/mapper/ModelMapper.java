package com.account.mapper;

import org.springframework.stereotype.Component;

import com.account.domain.Account;
import com.account.model.AccountModel;
@Component
public class ModelMapper {
	
	public AccountModel toModel(Account account) {
		AccountModel model = new AccountModel();
		model.setAccountID(account.getAccountID());
		model.setFirstName(account.getFirstName());
		model.setLastName(account.getLastName());
		model.setEmail(account.getEmail());
		model.setPhone(account.getPhone());
		model.setCompany(account.getCompany());
		model.setPassword(account.getPassword());
		return model;
	}

}
