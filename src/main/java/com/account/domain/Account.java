package com.account.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="ACCOUNTS",uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL"})})
public class Account {

	@Id
	@Column(name="ID_ACCOUNT",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long accountID;
	
	@Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="LAST_NAME")
    private String lastName;
    
    @Column(name="EMAIL")
    private String email;
    
    @Column(name="PHONE")
    private String phone;
    
    @Column(name="COMPANY")
    private String company;
    
    @Column(name="PASSWORD")
    private String password;

    
    public Account(String firstName, String lastName, String email, String phone, String company, String password) {
		this.firstName=firstName;
		this.lastName=lastName;
		this.email=email;
		this.phone=phone;
		this.company=company;
		this.password=password;
	}
    
    public Account() {}

	public long getAccountID() {
		return accountID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
	
    

}
